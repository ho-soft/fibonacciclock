﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FibonacciClock
{
	public class BerlinClockVM: ViewModelBase, IClockViewModel
	{
		private bool _sec;
		private bool _min1;
		private bool _min2;
		private bool _min3;
		private bool _min4;
		private bool _min5;
		private bool _min10;
		private bool _min15;
		private bool _min20;
		private bool _min25;
		private bool _min30;
		private bool _min35;
		private bool _min40;
		private bool _min45;
		private bool _min50;
		private bool _min55;
		private bool _hour1;
		private bool _hour2;
		private bool _hour3;
		private bool _hour4;
		private bool _hour5;
		private bool _hour10;
		private bool _hour15;
		private bool _hour20;
		private DateTime _time;

		public bool Second
		{
			get { return _sec; }
			set
			{
				if (_sec != value)
				{
					_sec = value;
					RaisePropertyChanged(nameof(Second));
				}
			}
		}

		public bool Min1
		{
			get { return _min1; }
			set
			{
				if (_min1 != value)
				{
					_min1 = value;
					RaisePropertyChanged(nameof(Min1));
				}
			}
		}

		public bool Min2
		{
			get { return _min2; }
			set
			{
				if (_min2 != value)
				{
					_min2 = value;
					RaisePropertyChanged(nameof(Min2));
				}
			}
		}

		public bool Min3
		{
			get { return _min3; }
			set
			{
				if (_min3 != value)
				{
					_min3 = value;
					RaisePropertyChanged(nameof(Min3));
				}
			}
		}

		public bool Min4
		{
			get { return _min4; }
			set
			{
				if (_min4 != value)
				{
					_min4 = value;
					RaisePropertyChanged(nameof(Min4));
				}
			}
		}

		public bool Min5
		{
			get { return _min5; }
			set
			{
				if (_min5 != value)
				{
					_min5 = value;
					RaisePropertyChanged(nameof(Min5));
				}
			}
		}

		public bool Min10
		{
			get { return _min10; }
			set
			{
				if (_min10 != value)
				{
					_min10 = value;
					RaisePropertyChanged(nameof(Min10));
				}
			}
		}

		public bool Min15
		{
			get { return _min15; }
			set
			{
				if (_min15 != value)
				{
					_min15 = value;
					RaisePropertyChanged(nameof(Min15));
				}
			}
		}

		public bool Min20
		{
			get { return _min20; }
			set
			{
				if (_min20 != value)
				{
					_min20 = value;
					RaisePropertyChanged(nameof(Min20));
				}
			}
		}

		public bool Min25
		{
			get { return _min25; }
			set
			{
				if (_min25 != value)
				{
					_min25 = value;
					RaisePropertyChanged(nameof(Min25));
				}
			}
		}

		public bool Min30
		{
			get { return _min30; }
			set
			{
				if (_min30 != value)
				{
					_min30 = value;
					RaisePropertyChanged(nameof(Min30));
				}
			}
		}

		public bool Min35
		{
			get { return _min35; }
			set
			{
				if (_min35 != value)
				{
					_min35 = value;
					RaisePropertyChanged(nameof(Min35));
				}
			}
		}

		public bool Min40
		{
			get { return _min40; }
			set
			{
				if (_min40 != value)
				{
					_min40 = value;
					RaisePropertyChanged(nameof(Min40));
				}
			}
		}

		public bool Min45
		{
			get { return _min45; }
			set
			{
				if (_min45 != value)
				{
					_min45 = value;
					RaisePropertyChanged(nameof(Min45));
				}
			}
		}

		public bool Min50
		{
			get { return _min50; }
			set
			{
				if (_min50 != value)
				{
					_min50 = value;
					RaisePropertyChanged(nameof(Min50));
				}
			}
		}

		public bool Min55
		{
			get { return _min55; }
			set
			{
				if (_min55 != value)
				{
					_min55 = value;
					RaisePropertyChanged(nameof(Min55));
				}
			}
		}

		public bool Hour1
		{
			get { return _hour1; }
			set
			{
				if (_hour1 != value)
				{
					_hour1 = value;
					RaisePropertyChanged(nameof(Hour1));
				}
			}
		}

		public bool Hour2
		{
			get { return _hour2; }
			set
			{
				if (_hour2 != value)
				{
					_hour2 = value;
					RaisePropertyChanged(nameof(Hour2));
				}
			}
		}

		public bool Hour3
		{
			get { return _hour3; }
			set
			{
				if (_hour3 != value)
				{
					_hour3 = value;
					RaisePropertyChanged(nameof(Hour3));
				}
			}
		}

		public bool Hour4
		{
			get { return _hour4; }
			set
			{
				if (_hour4 != value)
				{
					_hour4 = value;
					RaisePropertyChanged(nameof(Hour4));
				}
			}
		}

		public bool Hour5
		{
			get { return _hour5; }
			set
			{
				if (_hour5 != value)
				{
					_hour5 = value;
					RaisePropertyChanged(nameof(Hour5));
				}
			}
		}

		public bool Hour10
		{
			get { return _hour10; }
			set
			{
				if (_hour10 != value)
				{
					_hour10 = value;
					RaisePropertyChanged(nameof(Hour10));
				}
			}
		}

		public bool Hour15
		{
			get { return _hour15; }
			set
			{
				if (_hour15 != value)
				{
					_hour15 = value;
					RaisePropertyChanged(nameof(Hour15));
				}
			}
		}

		public bool Hour20
		{
			get { return _hour20; }
			set
			{
				if (_hour20 != value)
				{
					_hour20 = value;
					RaisePropertyChanged(nameof(Hour20));
				}
			}
		}

		public DateTime Time
		{
			get { return _time; }
			set { SetTime(value); }
		}

		private void SetTime(DateTime value)
		{
			if (_time.Second != value.Second)
			{
				_time = value;

				Second = (_time.Second & 1) != 0;

				int min = _time.Minute;
				Min55 = min >= 55;
				Min50 = min >= 50;
				Min45 = min >= 45;
				Min40 = min >= 40;
				Min35 = min >= 35;
				Min30 = min >= 30;
				Min25 = min >= 25;
				Min20 = min >= 20;
				Min15 = min >= 15;
				Min10 = min >= 10;
				Min5 = min >= 5;

				min = min % 5;
				Min4 = min >= 4;
				Min3 = min >= 3;
				Min2 = min >= 2;
				Min1 = min >= 1;

				int hour = _time.Hour;
				Hour20 = hour >= 20;
				Hour15 = hour >= 15;
				Hour10 = hour >= 10;
				Hour5 = hour >= 5;

				hour = hour % 5;
				Hour4 = hour >= 4;
				Hour3 = hour >= 3;
				Hour2 = hour >= 2;
				Hour1 = hour >= 1;

				RaisePropertyChanged(nameof(Time));
			}
		}
	}
}
