﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;
using System.Windows.Media;

namespace FibonacciClock
{
	class BerlinColorConverter: IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var val = value as bool?;
			if (val == true)
			{
				var propertyInfo = typeof(Brushes).GetProperty(parameter.ToString());
				if (propertyInfo != null)
				{
					return propertyInfo.GetValue(null, null);
				}
			}
			return Brushes.White;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
