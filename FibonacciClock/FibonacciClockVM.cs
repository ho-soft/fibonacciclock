﻿using System;
using System.ComponentModel;
using System.Timers;

namespace FibonacciClock
{
	/// <summary>
	/// View model for Fibonacci clock.
	/// </summary>
	public class FibonacciClockVM: ViewModelBase, IClockViewModel
	{
		private DateTime _time;
		private int _f0;
		private int _f1;
		private int _f2;
		private int _f3;
		private int _f4;
		private FibonacciConverter _hourConverter;
		private FibonacciConverter _minuteConverter;
		private int _hour = -1;
		private int _minute = -1;

		/// <summary>
		/// Color code for first digit (1)
		/// </summary>
		public int F0
		{
			get { return _f0; }
			set
			{
				if (_f0 != value)
				{
					_f0 = value;
					RaisePropertyChanged(nameof(F0));
				}
			}
		}

		/// <summary>
		/// Color code for second digit (1)
		/// </summary>
		public int F1
		{
			get { return _f1; }
			set
			{
				if (_f1 != value)
				{
					_f1 = value;
					RaisePropertyChanged(nameof(F1));
				}
			}
		}

		/// <summary>
		/// Color code for third digit (2)
		/// </summary>
		public int F2
		{
			get { return _f2; }
			set
			{
				if (_f2 != value)
				{
					_f2 = value;
					RaisePropertyChanged(nameof(F2));
				}
			}
		}

		/// <summary>
		/// Color code for fourth digit (3)
		/// </summary>
		public int F3
		{
			get { return _f3; }
			set
			{
				if (_f3 != value)
				{
					_f3 = value;
					RaisePropertyChanged(nameof(F3));
				}
			}
		}

		/// <summary>
		/// Color code for fifth digit (5)
		/// </summary>
		public int F4
		{
			get { return _f4; }
			set
			{
				if (_f4 != value)
				{
					_f4 = value;
					RaisePropertyChanged(nameof(F4));
				}
			}
		}

		/// <summary>
		/// Time to display.
		/// </summary>
		public DateTime Time
		{
			get { return _time; }
			set { SetTime(value); }
		}

		/// <summary>
		/// Constructor.
		/// </summary>
		public FibonacciClockVM()
		{
			_hourConverter = new FibonacciConverter();
			_minuteConverter = new FibonacciConverter();
			_time = new DateTime();
		}

		/// <summary>
		/// Set and encode time.
		/// </summary>
		/// <param name="time">Time to set.</param>
		/// <remarks>
		/// Hours are encoded as 0 to 12. Midnight is encoded as 0, noon
		/// as 12. Minutes are rounded down to the largest multiple of
		/// 5 not exceeding the actual minute. These values are encoded
		/// as sums of 1, 1, 2, 3 and 5.
		/// </remarks>
		private void SetTime(DateTime time)
		{
			_time = time;

			int hour = time.Hour;
			if (hour == 24)
				hour = 0;
			else if (hour > 12)
				hour -= 12;
			int minute = time.Minute / 5;

			if (hour != _hour || minute != _minute)
			{
				_hour = hour;
				_minute = minute;

				var h = FibonacciConverter.ToArray(_hourConverter.ToInt(hour));
				var m = FibonacciConverter.ToArray(_minuteConverter.ToInt(minute));

				F0 = h[0] * 2 + m[0];
				F1 = h[1] * 2 + m[1];
				F2 = h[2] * 2 + m[2];
				F3 = h[3] * 2 + m[3];
				F4 = h[4] * 2 + m[4];

				RaisePropertyChanged(nameof(Time));
			}
		}
	}
}