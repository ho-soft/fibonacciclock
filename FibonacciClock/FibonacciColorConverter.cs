﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace FibonacciClock
{
	/// <summary>
	/// Converter to convert color codes into colors.
	/// </summary>
	public class FibonacciColorConverter: IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var val = value as int?;
			if (val != null)
			{
				switch (val)
				{
				case 0:
					return Brushes.White;
				case 1:
					return Brushes.Red;
				case 2:
					return Brushes.Green;
				case 3:
					return Brushes.Blue;
				}
			}
			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
