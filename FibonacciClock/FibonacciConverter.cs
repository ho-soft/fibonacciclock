﻿using System;

namespace FibonacciClock
{
	/// <summary>
	/// Converter to encode numbers between 0 and 12 into sums of 1 (twice),
	/// 2, 3 and 5. These sums are not unique, multiple encodings of the
	/// same value can yield different results.
	/// </summary>
	public class FibonacciConverter
	{
		private int[] _current = new int[13];

		/// <summary>
		/// Converts an integer into a (not unique) sum. The summands used
		/// are represented by non-zero bits of the result.
		/// </summary>
		/// <param name="inVal">Plain number to encode.</param>
		/// <returns>Encoded number.</returns>
		public int ToInt(int inVal)
		{
			if (inVal < 0 || inVal > 12)
			{
				throw new ArgumentException(
                    string.Format(FibonacciRes.Resource.InvalidArgument, inVal)
					);
			}

			// Take the previous encoding of the specified number and find
			// its next encoding.
			int outVal = _current[inVal];
			do
			{
				if (++outVal > 31)
				{
					outVal = 0;
				}
			}
			while (Value(outVal) != inVal);
			_current[inVal] = outVal;

			return outVal;
		}

		/// <summary>
		/// Expand bits of an integral number into an array of zeros and
		/// ones.
		/// </summary>
		/// <param name="fibMask">Number to expand.</param>
		/// <returns>Expanded number.</returns>
		public static int[] ToArray(int fibMask)
		{
			var result = new int[5];

			result[0] = (fibMask & 1) / 1;
			result[1] = (fibMask & 2) / 2;
			result[2] = (fibMask & 4) / 4;
			result[3] = (fibMask & 8) / 8;
			result[4] = (fibMask & 16) / 16;

			return result;
		}

		/// <summary>
		/// Computes the numeric value of an encoded number.
		/// </summary>
		/// <param name="fibMask">Encoded number.</param>
		/// <returns>Numeric value of encoded number.</returns>
		private static int Value(int fibMask)
		{
			int value = 0;
			if ((fibMask & 1) != 0)
				value += 1;
			if ((fibMask & 2) != 0)
				value += 1;
			if ((fibMask & 4) != 0)
				value += 2;
			if ((fibMask & 8) != 0)
				value += 3;
			if ((fibMask & 16) != 0)
				value += 5;
			return value;
		}
	}
};