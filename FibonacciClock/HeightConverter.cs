﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace FibonacciClock
{
	/// <summary>
	/// Calculates the height of the window from its width. The width to
	/// height ratio should be 8:5.
	/// </summary>
	public class HeightConverter: IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var iVal = System.Convert.ToInt32(value, CultureInfo.InvariantCulture);
			var iPar = System.Convert.ToDouble(parameter, CultureInfo.InvariantCulture);
			return (int)(iVal * iPar);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
