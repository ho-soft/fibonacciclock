﻿using System;
using System.ComponentModel;

namespace FibonacciClock
{
	public interface IClockViewModel: INotifyPropertyChanged
	{
		DateTime Time { get; set; }
	}
}