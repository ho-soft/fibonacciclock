﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace FibonacciClock
{
	/// <summary>
	/// Implementation of main window.
	/// 
	/// Additionally to other resposibilities of a main window implementation,
	/// this class is also resposible to trigger time updates. To accomplish
	/// this task, it creates a timer and whenever a change of minutes has
	/// been detected, the fibonacci view model is updated.
	/// 
	/// This class is also responsible to update the icon, whenever the clock
	/// display changes.
	/// </summary>
	public partial class MainWindow: Window, INotifyPropertyChanged
	{
		private IClockViewModel _vm = new FibonacciClockVM();
		private int _sec = -1;
		private System.Timers.Timer _timer;

		public IClockViewModel VM
		{
			get { return _vm; }
			set
			{
				if (_vm != value)
				{
					if (_vm != null)
					{
						_vm.PropertyChanged -= OnPropertyChangedHandler;
					}
					_vm = value;
					_vm.PropertyChanged += OnPropertyChangedHandler;
					RaisePropertyChanged(nameof(VM));
				}
			}
		}

		public ICommand SelectBerlinClock
		{
			get { return new RelayCommand(RunSelectBerlinClock); }
		}

		public ICommand SelectFibonacciClock
		{
			get { return new RelayCommand(RunSelectFibonacciClock); }
		}

        public ICommand SelectEnglish
        {
            get { return new RelayCommand(RunSelectEnglish); }
        }

        public ICommand SelectGerman
        {
            get { return new RelayCommand(RunSelectGerman); }
        }

        private void RunSelectGerman()
        {
            SelectLanguage("de-DE");
        }

        private void RunSelectEnglish()
        {
            SelectLanguage("en-US");
        }

        private void SelectLanguage(string loc)
        {
            var cul = CultureInfo.CreateSpecificCulture(loc);
            Thread.CurrentThread.CurrentUICulture = cul;
        }

        private void RunSelectFibonacciClock()
		{
			VM = new FibonacciClockVM();
		}

		private void RunSelectBerlinClock()
		{
			VM = new BerlinClockVM();
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public MainWindow()
		{
			InitializeComponent();

			DataContext = this;
			_vm.PropertyChanged += OnPropertyChangedHandler;

			_vm.Time = DateTime.Now;

			_timer = new System.Timers.Timer(100);
			_timer.Elapsed += (sender, e) => Dispatcher?.Invoke(OnTimer);
			_timer.Start();
		}

		/// <summary>
		/// Relevant time components have been changed.
		/// 
		/// This function is called whenever the FibonacciClockVM.Time has
		/// been changed. It is responsible to update the program's icon.
		/// </summary>
		/// <remarks>
		/// When Time has been changed, the control has not been updated.
		/// If this function would update the icon immediately, the icon
		/// would show the previous time. To show the proper item, calling
		/// InvalidateIcon must be delayed until the main windows has been
		/// updated and the thread is idle again.
		/// </remarks>
		private void OnPropertyChangedHandler(object sender, PropertyChangedEventArgs e)
		{
			if (sender == _vm && e.PropertyName == "Time")
			{
				Dispatcher.BeginInvoke(
					new Action(()=>InvalidateIcon()),
					DispatcherPriority.ContextIdle);
			}
		}

		/// <summary>
		/// Invalidate the program's icon.
		/// </summary>
		/// <remarks>
		/// There's not much to do right now. Just tell the window (or whoever
		/// cares), that the image has been changed.
		/// </remarks>
		private void InvalidateIcon()
		{
			RaisePropertyChanged(nameof(ImageSource));
		}

		/// <summary>
		/// Image to use as the program's icon.
		/// </summary>
		/// <remarks>
		/// The image is created dynamically whenever this property is read.
		/// The image will be created from the current view of the clock
		/// control.
		/// </remarks>
		public BitmapSource ImageSource
		{
			get
			{
				var bmp = new RenderTargetBitmap(512, 512, 96, 96, PixelFormats.Pbgra32);
				bmp.Render(ctrlClock);
				return bmp;
			}
		}

		private void OnTimer()
		{
			var time = DateTime.Now;
			if (time.Second != _sec)
			{
				_sec = time.Second;
				_vm.Time = time;
			}
		}

		private void RaisePropertyChanged(string propName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
		}

		private void OnClosing(object sender, CancelEventArgs e)
		{
			_timer.Stop();
		}

		private void OnClickExit(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
