﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FibonacciClock
{
	public class RelayCommand: ICommand
	{
		public event EventHandler CanExecuteChanged
		{
			add { CommandManager.RequerySuggested += value; }
			remove { CommandManager.RequerySuggested -= value; }
		}

		private Action _methodToExecute;
		private Action<object> _methodWithParamToExecute;
		private Func<bool> _canExecuteEvaluator;
		private Func<object, bool> _canExecuteEvaluatorParam;

		/// <summary>
		/// RelayCommand mit der Methode zum Ausführen und einer Funktion zum Feststellen, ob es getan werden kann
		/// </summary>
		/// <param name="methodToExecute">die Methode</param>
		/// <param name="canExecuteEvaluator">Funktion zum Prüfen, kann es getan werden?</param>
		public RelayCommand(Action methodToExecute, Func<bool> canExecuteEvaluator)
		{
			_methodToExecute = methodToExecute;
			_canExecuteEvaluator = canExecuteEvaluator;
		}

		/// <summary>
		/// RelayCommand mit der Methode zum Ausführen
		/// </summary>
		/// <param name="methodToExecute">die Methode</param>
		public RelayCommand(Action methodToExecute)
			: this(methodToExecute, null)
		{ }

		/// <summary>
		/// RelayCommand mit der Methode als Object zum Ausführen
		/// </summary>
		/// <param name="methodToExecute">die Methode als Object</param>
		public RelayCommand(Action<object> methodToExecute)
		{
			this._methodWithParamToExecute = methodToExecute;
		}

		/// <summary>
		/// RelayCommand mit der Methode zum Ausführen (hier als Objekt übergeben) und einer Funktion zum Feststellen, ob es getan werden kann
		/// </summary>
		/// <param name="methodToExecute">die Methode als Object</param>
		/// <param name="canExecuteEvaluator">Funktion zum Prüfen, kann es getan werden?</param>
		public RelayCommand(Action<object> methodToExecute, Func<bool> canExecuteEvaluator)
		{
			_methodWithParamToExecute = methodToExecute;
			_canExecuteEvaluator = canExecuteEvaluator;
		}

		/// <summary>
		/// RelayCommand mit der Methode zum Ausführen(hier als Objekt übergeben)  und einer Funktion zum Feststellen, ob es getan werden kann
		/// </summary>
		/// <param name="methodToExecute">die Methode als Object</param>
		/// <param name="canExecuteEvaluator">Funktion zum Prüfen, kann es getan werden, auch mit Objekt</param>
		public RelayCommand(Action<object> methodToExecute, Func<object, bool> canExecuteEvaluator)
		{
			_methodWithParamToExecute = methodToExecute;
			_canExecuteEvaluatorParam = canExecuteEvaluator;
		}


		/// <summary>
		/// Kann diese Methode für diesen Parameter ausgeführt werden
		/// </summary>
		/// <param name="parameter">Parameter zum Testen</param>
		/// <returns></returns>
		public bool CanExecute(object parameter)
		{
			if (_canExecuteEvaluator != null)
			{
				return _canExecuteEvaluator.Invoke();
			}
			else if (_canExecuteEvaluatorParam != null)
			{
				return _canExecuteEvaluatorParam.Invoke(parameter);
			}

			return true;
		}

		/// <summary>
		/// Jetzt die Methode ausführen
		/// </summary>
		/// <param name="parameter"></param>
		public void Execute(object parameter)
		{
			if (parameter == null)
			{
				_methodToExecute.Invoke();
			}
			else
			{
				_methodWithParamToExecute.Invoke(parameter);
			}
		}
	}
}
