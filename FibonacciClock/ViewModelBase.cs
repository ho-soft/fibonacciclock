﻿using System.ComponentModel;

namespace FibonacciClock
{
	public class ViewModelBase: INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Notify that a property's value has been changed.
		/// </summary>
		/// <param name="propName">Name of property.</param>
		public void RaisePropertyChanged(string propName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
		}
	}
}
