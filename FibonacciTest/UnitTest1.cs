﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FibonacciClock;

namespace FibonacciTest
{
	[TestClass]
	public class UnitTest1
	{
		[TestMethod]
		public void TestMethod1()
		{
			var conv = new FibonacciConverter();
			var value1 = conv.ToInt(5);	// 11010
			var value2 = conv.ToInt(5); // 00110
			var value3 = conv.ToInt(5); // 00001
			var value4 = conv.ToInt(5); // 11010
			Assert.AreEqual(11, value1);
			Assert.AreEqual(12, value2);
			Assert.AreEqual(16, value3);
			Assert.AreEqual(11, value4);
		}
	}
}
