# README #

This little program simulates a clock based on Fibonacci numbers. Hours and minutes (only multiples of
5 minutes) are displayed in five fields which represent the first five Fibonacci numbers 1, 1, 2, 3 and 5.
Hours and 5-minutes are expressed as sums of these numbers and the color of each field tells whether its
number is used in the sums. Hence, each field can show one of four colors.

-	White  
	The number is not used in any sum.
-	Red  
	The number is used for minutes only.
-	Green  
	The number is used for hours only.
-	Blue  
	The number is used for both hours and minutes.

## Version 1.0.0.1 ###

Added the *Berlin Clock* as an alternative to *Fibonacci Clock*.

The Berlin Clock had been located near *Europa Center* at the end of
*Kurfürstendamm*. It displayed the time using many lights arranged in
four rows.

-   The first row had 4 red lights each standing for 5 full hours each.
-   The second row also had 4 red lights standing for 1 full hour each.
-   The third row had 8 orange and 3 read lights standing for 5 full
	minute each. Red lights are used to highlight 15, 30 and 45 minutes.
-   The fourth row had 4 orange lights standing for 1 full minute each.

To read the hour you must count the number of switched on lights in the
first to rows and calculate the hour as

	hour = 4 * lights in 1st row + lights in 2nd row

For the minutes the number of switched on lights in the last two rows
must be counted.

	minute = 5 * lights in 3rd row + lights in 4th row

On top of the structure was a round light, which was off for one second,
on for the next second, and then repeating this cycle.

*EOF*